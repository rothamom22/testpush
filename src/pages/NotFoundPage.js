import React from 'react'

const NotFoundPage = () => {
    return (
        <div className='container mt-5'>

            <h1 className='text-center text-danger'> 404 NOT FOUND!!!!</h1>
        </div>
    )
}

export default NotFoundPage