import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { NavLink } from 'react-router-dom';

function UserCard({userInfo}) {
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={userInfo.avatar } />
      <Card.Body>
        <Card.Title>{userInfo.name} <span className = 'text-warning'>{userInfo.role} </span> </Card.Title>
        <Card.Text>
        {userInfo.email}
        </Card.Text>
        <NavLink to={`/user/${userInfo.id}`} >
        <Button variant="primary">View</Button>
        </NavLink>
      </Card.Body>
    </Card>
  );
}

export default UserCard;