import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { NavLink } from 'react-router-dom';

function MyNavBaar() {
  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand href="#home">Rotha</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <NavLink className = " nav-Link " to= {"/"}> Home </NavLink>
            <NavLink className = " nav-Link " to= {"/user"} > User </NavLink>
            {/* <NavLink className = " nav-Link " to= {"/server"} > Server </NavLink> */}
            <NavLink className = " nav-Link" to= {"/product"} > Product </NavLink>
           
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default MyNavBaar;