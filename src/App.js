import logo from './logo.svg';
import './App.css';
import { BrowserRouter as BigRouter, Route, Routes } from 'react-router-dom';
import HomePage from './pages/HomePage';
import "bootstrap/dist/css/bootstrap.min.css"
import UserPage from './pages/UserPage';
import ProductPage from './pages/ProductPage';
import NotFoundPage from './pages/NotFoundPage';
import { useEffect } from 'react';
import ViewUserProfile from './pages/ViewUserProfile';
import MyNavBar from './components/MyNavBar';

function App() {



  return (
    <BigRouter>
      <MyNavBar />
      <Routes>
        <Route path='/' index element={<HomePage />} />
        <Route path='/user' element={<UserPage />} />
        {/* <Route path='/product' element={<ProductPage />} />
        <Route path='/user/:id' element={<ViewUserProfile />} />
        <Route path='*' element={<NotFoundPage />} /> */}
      </Routes>
    </BigRouter>
  );
}

export default App;